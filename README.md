# ansible-selfhosted

**WARNING:** This is a codename and will change.

In this repository, you will find all you need to setup your own server. It contains:

- [Ansible](https://en.wikipedia.org/wiki/Ansible_(software)) roles to implement the services
- a [deploy.sh](deploy.sh) script to apply these settings on a server (run as root)

# Setup

To configure your server, you will need a dedicated folder to keep track of all the configuration. It is recommended to version this folder with git or mercurial (or the tool of your choice). This repository should then be cloned as a submodule in the `roles` folder. Assuming you want to store server data in `/etc/server`, you could do:

```
# mkdir /etc/server; cd /etc/server
# git init
# git submodule add https://codeberg.org/southerntofu/ansible-selfhosted /etc/server/roles
```

Now, you should:

- copy the [deploy.sh](deploy.sh) script to your server's folder
- configure the server in `config.yml`
  * here's some example configs: [joinjabber.org](https://codeberg.org/joinjabber/infra/src/branch/main/config.yml)
  * the complete configuration format is explained in this README, as well as in the README of individual roles linked in this document
- run `./deploy.sh`
- enjoy?

Your server configuration folder should look like this in the end:

- `config.yml`
- `deploy.sh`
- `roles/`

# Services

We currently support the following high-level services:

- `webserver` ([docs](webserver/README.md), [todo](https://codeberg.org/joinjabber/infra/issues?labels=12554)): setup static websites (with git source and build command) or reverse proxies, with automatic TLS/Tor support
- **WIP** `mailserver` ([docs](mailserver/tasks/main.yml), [todo](https://codeberg.org/joinjabber/infra/issues?labels=12555)): setup mailboxes and aliases, with automatic TLS/Tor support
- `jabberserver` ([docs](jabberserver/README.md), [todo](https://codeberg.org/joinjabber/infra/issues?labels=12556)): setup MUC servers and/or anonymous/guest vhosts, with automatic TLS/Tor support
- `chatbridge` ([docs](chatbridge/README.md)): a bot to bridge connections across instant messaging networks (IRC, Matrix, Jabber/XMPP) such as [matterbridge](https://github.com/42wim/matterbridge)

Some roles are intended as building blocks for other roles, though they may come in handy in specific situations:

- `tls`: request a TLS certificate from [letsencrypt](https://letsencrypt.org/) for a domain, and optionally some aliases ([docs](tls/README.md), [todo](https://codeberg.org/joinjabber/infra/issues?labels=12557))
- `tor`: setup a Tor onion service acting as a reverse proxy for a local service ([docs](tor/README.md), [todo](https://codeberg.org/joinjabber/infra/issues?labels=12553))

Some additional roles we'd like to support in the future:

- `nameserver`: setup primary/secondary DNS server

There is also some roles acting as package managers, as explained later in this document.

# Configuration

The configuration file from the server is `config.yml`. In addition to specific role configuration, there's a number of high-level settings in there, explained in this section.

## Base settings

In order to operate properly, the server needs a certain number of base settings:

- `hostname`: the main domain name for the server
- `aliases`: a list of domain names also vaild for this server, which will be included in the main TLS certificate
- `contact`: contact information used by some roles
  - `email`: a list of contact emails (default: `contact@` followed by `hostname`)
  - `xmpp_muc`: a list of Jabber/XMPP chatrooms (default: `None`)
  - `xmpp_users`: a list of Jabber/XMPP users (default: `None`)

## Packages

**TODO:** Package managers should be better documented and their interface standardized. WTF does go not accept https://github.com/ as valid repository source and require to strip the scheme?

The configuration file contains a `packages` field, which is a mapping of package managers to a list of packages for them to install. How the list of packages is treated may be different for every package manager. For example:

```
packages:
  debian: [ "tmux" ]
  rust:
    - "lsd"
    - bin: "zola"
      repo: "https://github.com/southerntofu/zola"
      version: "bugfix-index-translations"
```

This configuration snippet above will setup three packages:

- tmux from the Debian repositories
- lsd from [crates.io](https://crates.io)
- zola from [github.com/southerntofu/zola](https://github.com/southerntofu/zola) on the `bugfix-index-translations` branch

Internally, every package manager is a role prefixed by a dot, to avoid confusion with other roles (services). We currently support the following package managers:

- debian ([docs](.debian/tasks/main.yml))
- rust ([docs](.rust/tasks/main.yml))
- go ([docs](.go/tasks/main.yml))

**Note:** `.common` is a reserved role name, and cannot be used for a `common` package manager.

## Services

The configuration file has a `services` field, which is a list of services (roles) to setup on the server. In most cases, it should contain the `.common` role, which will perform the base setup. For example:

```
services:
  - ".common"
  - "webserver"
```

This snippet above will configure the base server, as well as a webserver. The webserver configuration, like any other service configuration, is kept in a separate field so the services remains human-readable. The field containing the configuration for a specific service wears the same name as the service (role) itself. For example, webserver configuration lives in a `webserver` field in the configuration file. Inside a service configuration, global settings are usually defined in a `settings` key, for example `webserver.settings`. 

## Virtualhosts

Most services can be configured to serve different domains/users. These specific configuration blocks are separated into what we call virtualhosts, or vhosts for short. Each such vhost for a service is configured in the `vhosts` list, inside the service configuration block. For example, `webserver.vhosts`. How to configure a vhost for a specific service is left to interpretation, and you should refer to service docs in order to find out what kind of settings a vhost may accept/require, and what the service will do with those settings.

**Note:** Some services may use similar concepts, though not called "vhosts". For example, `chatbridge` service uses `chans` for multiple configurations.

## Profiles {#profiles}

A service may support different use-cases for each vhost. While these are usually defined using the `template` setting of a vhost, we provide a high-level shorthand method for defining separate profiles without typing it out for each individual vhost. Each service profile is a top-level configuration key inside the service configuration, wearing the name of the service profile, and containing a list of vhosts which will be loaded with `template` set to this profile name. Take the following configuration snippet:

```
webserver:
  vhosts:
    - host: "example.org"
      template: "static"
      git: "https://git.example.org/foo/bar"
    - host: "example.net"
      template: "static"
      git: "https://git.example.net/foo/bar"
```

The above snippet is strictly equivalent to:

```
webserver:
  static:
    - host: "example.org"
      git: "https://git.example.org/foo/bar"
    - host: "example.net"
      git: "https://git.example.net/foo/bar"
```

# Deploying

A [deploy.sh](deploy.sh) script is provided to make deploying easier. It's intended to be run as root from the server itself, and optionally accepts positional arguments representing a list of services to (re)configure, instead of following the services list defined in the configuration file:

```
# # Setup the server according to config.yml
# ./deploy.sh
# # Only reconfigure webserver
# ./deploy.sh webserver
```

# Documentation

This document is the top-level documentation. In addition, you may find documentation about:

- services:
  - [webserver](webserver/README.md)
  - **TODO** [mailserver](mailserver/README.md)
  - [jabberserver](jabberserver/README.md)
  - [tls](tls/README.md)
  - [tor](tor/README.md)
  - [chatbridge](chatbridge/README.md)

# Copyleft (license)

This project is protected by [AGPLv3 license](LICENSE). We believe the fruit of human labor should belong to humanity as a whole, and privatization of resources and knowledge is harmful. If you don't have time to read the full license, you may:

- redistribute this project with or without further modification, under the same license
- use this project to serve your personal needs, without restriction (if the user-facing services are used by anyone else but you, it is not considered personal usage and falls under the next category)
- use this project to help/serve other persons, under the condition that you share every modification publicly under the same license
