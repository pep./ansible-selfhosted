{%- set default_muc = item.default_muc|default(jabberserver.settings.default_muc|default(None)) -%}
// eslint-disable-next-line no-unused-vars, no-var
var config = {
  name: 'XMPP web',
  transports: {
    websocket: 'wss://{{ host }}/xmpp-websocket',
    bosh: 'https://{{ host }}/http-bind',
  },
  hasGuestAccess: true,
  hasRegisteredAccess: false,
  anonymousHost: '{{ host }}',
  isTransportsUserAllowed: false,
  hasHttpAutoDiscovery: false,
  resource: 'Web XMPP',
  defaultDomain: '{{ host }}',
  defaultMuc: '{{ default_muc }}',
  isStylingDisabled: false,
}
