Component "{{ item.host }}" "muc"
{% if item.admins|default(None) %}
  admins = { {% for admin in item.admins -%}"{{ admin }}"{% if not loop.last %}, {% endif %}{% endfor %} };
{% endif %}

{% if item.contact|default(None) %}
{# When no contact_info is specified on a vhost, prosody will advertise main contact_info #}
contact_info = {
  abuse = { {% for addr in item.contact %}"{{ addr }}"{% if not loop.last %}, {% endif %}{% endfor %} };
  admin = { {% for addr in item.contact %}"{{ addr }}"{% if not loop.last %}, {% endif %}{% endfor %} };
}
{% endif %}

  ssl = {
    certificate = "/etc/letsencrypt/live/{{ item.host }}/fullchain.pem";
    key = "/etc/letsencrypt/live/{{ item.host }}/privkey.pem";
  }

  modules_enabled = {
    "muc_mam",
    "ping",
    "bidi",
    "server_contact_info",
    "vcard_muc",
  };
  -- TODO: creation settings!
  restrict_room_creation = 'local';
  -- TODO: configurable logs
  muc_log_all_rooms = true;
