{%- set web_client = item.web_client|default(jabberserver.settings.web_client|default(None)) -%}
{%- set default_muc = item.default_muc|default(jabberserver.settings.default_muc|default(None)) -%}

VirtualHost "{{ item.host }}"
  authentication = "anonymous"
{% if item.admins|default(None) %}
  admins = { {% for admin in item.admins -%}"{{ admin }}"{% if not loop.last %}, {% endif %}{% endfor %} };
{% endif %}

{% if item.contact|default(None) %}
{# When no contact_info is specified on a vhost, prosody will advertise main contact_info #}
contact_info = {
  abuse = { {% for addr in item.contact %}"{{ addr }}"{% if not loop.last %}, {% endif %}{% endfor %} };
  admin = { {% for addr in item.contact %}"{{ addr }}"{% if not loop.last %}, {% endif %}{% endfor %} };
}
{% endif %}

{% if default_muc %}
  disco_items = { { "{{ default_muc }}", "Chatrooms" } }
{% endif %}

  ssl = {
    certificate = "/etc/letsencrypt/live/{{ item.host}}/fullchain.pem",
    key = "/etc/letsencrypt/live/{{ item.host }}/privkey.pem"
  }

{% if web_client %}
  http_external_url = "{{ item.host }}"
  http_paths = {
    bosh = "/http-bind";
    websocket = "/xmpp-websocket";
  }

  modules_enabled = {
    "bosh",
    "websocket"
  }
{% endif %}
