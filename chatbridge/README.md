# chatbridge

This role configures a certain number of chatbots to relay messages across the following networks:

- [x] Jabber/XMPP
- [x] IRC
- [ ] matrix

## Arguments

This role will configure some chans, defined in `chatbridge.chans` variable. Each chan consists of the following arguments:

- **(required)** `name`: a unique identifier for this chan
- **(required)** `chans`: a mapping from account name to a channel name

Account configuration is described in the [Settings](#settings) section.

## Settings

The `chatbridge` role settings are defined in the configuration file, under the `chatbridge.settings` key. The following settings are available:

- **(required)** `accounts`: a dictionary of accounts, per protocol
  - `xmpp`: a dictionary of named XMPP accounts
  - `irc`: a dictionary of named IRC accounts
- `nick`: the bot's nick (default: `relay`)
- `format`: the message format (default: `[{PROTOCOL}] <{NICK}>`)

Each account must have the following **required** settings:

- `name`: a unique identifier for this account, as used by chan declarations
- `type`: the protocol used by this account  (`irc` or `xmpp`)
- `login`: the username to connect with to the server ; for XMPP accounts, the client's server is determined by this variable
- `server`: the hostname of the server to connect to ; for XMPP, this refers to the MUC server for chatrooms, and the client's server is configured in `login` setting

**Notes:**

- passwords are stored plaintext in `/opt/chatbridge/passwords/` folder, in a file named after the account name
- for XMPP accounts, the actual credentials are stored in `/opt/chatbridge/passwords/xmpp_JID`, where `JID` is the actual login (multiple MUC "accounts" can use the same login)

## Conventions

In order to interface with other roles/services, this role follows a certain number of conventions:

## TODO

- [ ] Support [multiple systemd profiles](https://www.stevenrombauts.be/2019/01/run-multiple-instances-of-the-same-systemd-unit/) to run parallel instances of matterbridge
