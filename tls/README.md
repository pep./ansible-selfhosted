# tls

This service role will request a TLS certificate from [Letsencrypt](https://letsencrypt.org/) for a given domain, with aliases support.

## Arguments

This role will request a single TLS certificate. It takes the following arguments:

- **(required)** `tls_host`: the primary hostname for the certificate, which will be used as identifier
- `tls_aliases`: an optional list of aliases that should be contained in the certificate

**Notes:**

- if you add an alias after the domain has been generated, a new certificate will not be regenerated; however, the alias will be included in the new certificate upon renewal
- currently contact information is not setup automatically, so the task run may request your input at runtime

## Settings

- **TODO** `contact`: override default server contact information
- **TODO** `rsa_key_size`: RSA key length (default: `4096`)
No settings yet.

## Conventions

In order to interface with other roles/services, this role follows a certain number of conventions:

- `/etc/letsencrypt/live/HOST/` will contain the private/public key for `HOST`
- `/etc/letsencrypt/renewal-hooks/post/` contains the scripts that should be executed upon renewing a certificate, as populated by other roles

## TODO

- [ ] Support adding aliases after the fact
- [ ] Support multiple vhosts
- [ ] Support other ACME providers (not just letsencrypt)
