---

- name: "Setup tor from backports"
  apt:
    default_release: "buster-backports"
    name: "tor"
    state: "latest"
  when: ansible_distribution == "Debian"

- name: "Setup tor"
  package:
    name: "tor"
    state: "latest"
  when: ansible_distribution != "Debian"

- set_fact:
    tor_user: "{{ (ansible_distribution == 'Debian') | ternary('debian-tor', 'tor') }}"

- name: "Create necessary folders"
  file:
    path: "{{ item.dest }}"
    state: "directory"
    owner: "{{ item.owner | default(tor_user) }}"
    group: "{{ item.owner | default(tor_user) }}"
    mode: "{{ item.mode | default('0740') }}"
  loop:
    - { dest: "/etc/tor/onions" }
    - { dest: "/var/log/tor" }
    - { dest: "/etc/systemd/system/tor.service.d", owner: "root", group: "root", mode: '0644' }

- name: "Ensure tor daemon is using the proper user"
  copy:
    content: "[Service]\nUser={{ tor_user }}\n"
    dest: "/etc/systemd/system/tor.service.d/user.conf"
    owner: "root"
    group: "root"
    mode: "0644"
  register: override

- name: "Reload systemd"
  systemd:
    daemon-reload: true
  when: override.changed

- name: "Configure tor daemon"
  template:
    src: "files/torrc.j2"
    dest: "/etc/tor/torrc"
  notify: "reload tor"

# TODO: This is a workaround because default apparmor profile is broken on buster and prevents tor from loading other configuration files in /etc/tor/FOOBAR/
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=888728
- name: "Ensure tor can read config (apparmor)"
  lineinfile:
    path: "/etc/apparmor.d/abstractions/tor"
    regexp: "/etc/tor/\\* r,"
    line: "/etc/tor/** r,"
    state: "present"
  register: apparmor
  when: ansible_distribution == 'Debian'

- name: "Restart apparmor"
  service:
    name: "apparmor"
    state: "restarted"
  when: apparmor.changed

# First we load vhosts from config so their configuration has precedence
# vhosts loaded from config will only have onion generated at the end of the playbook (in the "reload tor" handler)
# if you need knowledge of the tor onion, please pass variable now: true to this role
- name: "Configure vhosts from config"
  include: "vhost.yml"
  loop: "{{ tor.vhosts|default([]) }}"
  notify: "reload tor"
  register: "config_file"

- name: "Configure vhosts passed from another role"
  include: "vhost.yml"
  loop: "{{ vhosts|default([]) }}"
  notify: "reload tor"
  register: "config_role"

# When now == true, we restart tor immediately so onion names are available to other roles (for vhost configuration)
- name: "Wait for onions to be generated passed from another role"
  wait_for:
    path: "/var/lib/tor/{{ item.name }}/hostname"
  loop: "{{ vhosts|default([]) }}"
  when: now and config_role.changed

- name: "Wait for onions to be generated from config"
  wait_for:
    path: "/var/lib/tor/{{ item.name }}/hostname"
  loop: "{{ tor.vhosts|default([]) }}"
  when: now and config_file.changed
